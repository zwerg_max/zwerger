# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit qmake-utils

DESCRIPTION="SDDM Configuration Editor"
HOMEPAGE="https://github.com/lxqt/sddm-config-editor.git"

if [[ ${PV} = *9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/lxqt/${PN}.git"
fi

LICENSE="Apache-2.0"
SLOT="0"

DEPEND="
	dev-qt/qtquickcontrols:5
	>=x11-misc/sddm-0.18.0
	sys-auth/polkit
"
RDEPEND="${DEPEND}"

src_configure() {
	cd "$srcdir/$_pkgname/cpp"
	qmake PREFIX="$pkgdir"
	make
}

src_install() {
	cd "$srcdir/$_pkgname/cpp"
	make install
}