# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

DESCRIPTION="GUI configuration tool for the compton X composite manager"
HOMEPAGE="https://github.com/lxqt/compton-conf"

SRC_URI="https://github.com/lxqt/${PN}/releases/download/${PV}/${P}.tar.xz"
KEYWORDS="~amd64 ~x86"

LICENSE="LGPL-2.1"
SLOT="0"
IUSE=""

RDEPEND="
	x11-misc/compton
	dev-qt/qtcore:5
	dev-libs/libconfig
"
DEPEND="${RDEPEND}
	dev-qt/linguist-tools:5
	>=dev-util/lxqt-build-tools-0.8.0
"

src_configure() {
	local mycmakeargs=( -DPULL_TRANSLATIONS=OFF	)
	cmake_src_configure
}