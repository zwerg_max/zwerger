# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PVCUT=$(ver_cut 1-3)
KFMIN=5.96.0
QTMIN=5.15.5
inherit ecm gear.kde.org

DESCRIPTION="KDE Telepathy UI for audio/video calls"
HOMEPAGE="https://community.kde.org/KTp"

LICENSE="Apache-2.0 || ( AFL-2.1 BSD ) GPL-2+ LGPL-2.1+ MIT"
SLOT="5"
KEYWORDS="~amd64 ~x86"

DEPEND="
	>=kde-frameworks/extra-cmake-modules-${KFMIN}:5
	>=kde-frameworks/kconfig-${KFMIN}:5
	>=kde-frameworks/kdeclarative-${KFMIN}:5
	>=kde-frameworks/kiconthemes-${KFMIN}:5
	>=kde-frameworks/ki18n-${KFMIN}:5
	>=kde-frameworks/knotifications-${KFMIN}:5
	>=kde-frameworks/kxmlgui-${KFMIN}:5
	>=kde-frameworks/kcmutils-${KFMIN}:5
	>=kde-apps/kaccounts-integration-${PVCUT}:5
	>=kde-apps/ktp-common-internals-${PVCUT}:5[otr]
	media-libs/phonon
	>=net-libs/telepathy-qt-0.9.8[farstream]
	>=dev-qt/qtcore-${QTMIN}:5
	>=dev-qt/qtgui-${QTMIN}:5
	>=dev-qt/qtdeclarative-${QTMIN}:5
"
BDEPEND="virtual/pkgconfig"
