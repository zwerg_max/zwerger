# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit rpm

DESCRIPTION="R-Linux is a file recover utility."
HOMEPAGE="http://www.r-tt.com"
SRC_URI="
	x86? ( https://www.r-studio.com/downloads/${PN}_i386.rpm )
	amd64? ( https://www.r-studio.com/downloads/${PN}_x64.rpm )
"

KEYWORDS="~amd64 ~x86"
LICENSE="R-TT-EULA"
SLOT="4.9"
IUSE=""
RESTRICT="mirror"

DEPEND="
	x11-misc/xdg-utils
	app-arch/rpm2targz
"
RDEPEND=">=x11-base/xorg-server-1.7.6"

S="${WORKDIR}"

src_unpack () {
	rpm_src_unpack ${A}
	cd "${S}"
}

src_install() {
	cp -pPR "${S}/usr" "${D}"/
}

pkg_postrm() {
	xdg_icon_cache_update
}

pkg_postinst() {
	xdg_icon_cache_update
}