# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit fdo-mime font gnome2-utils eutils multilib unpacker

DESCRIPTION="Sputnik webrowser"
HOMEPAGE="https://browser.sputnik.ru/"

KEYWORDS="~amd64"

SRC_URI="
    amd64? ( http://browser-deb.sputnik.ru/ubuntu/pool/main/s/${PN}-stable/${PN}-stable_${PV}-1_amd64.deb )
    "

SLOT="0"
RESTRICT="strip mirror"
LICENSE="AGPL-3"
IUSE=""


NATIVE_DEPEND="gnome-base/gconf
"
RDEPEND="
    ${NATIVE_DEPEND}
"
DEPEND="${RDEPEND}"

S="${WORKDIR}"

src_unpack(){
        unpack_deb ${A}
}

src_install() {
	cp -R "${WORKDIR}/etc" "${D}" || die "install failed!"
	cp -R "${WORKDIR}/opt" "${D}" || die "install failed!"
	cp -R "${WORKDIR}/usr" "${D}" || die "install failed!"

	local size
	for size in 16 22 24 32 48 64 128 256; do
		dosym ../../../../../../opt/${PN}/product_logo_${size}.png \
			/usr/share/icons/hicolor/${size}x${size}/apps/sputnik-browser.png
	done
}

pkg_postrm() {
	xdg_icon_cache_update
}

pkg_postinst() {
	xdg_icon_cache_update
}