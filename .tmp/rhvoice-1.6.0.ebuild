# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{8,9} )

inherit python-any-r1 scons-utils toolchain-funcs multilib

DESCRIPTION="A speech synthesizer for Russian (and similar) language"
HOMEPAGE="https://github.com/RHVoice/RHVoice"
SRC_URI="${HOMEPAGE}/releases/download/${PV}/${P}.tar.gz"

KEYWORDS="~amd64"

LICENSE="GPL-3"
SLOT="0"

RDEPEND=""
DEPEND="
	${RDEPEND}
	dev-python/pkgconfig
	dev-util/scons
	app-accessibility/speech-dispatcher
	app-accessibility/flite
	media-sound/pulseaudio
	media-sound/sox
	dev-libs/libunistring
	dev-libs/expat
	dev-libs/libpcre
"

DOCS=(README.md doc)

# TODO: multilib support (just in case)
src_unpack() {
	unpack ${A}
}

src_compile() {
	#escons DESTDIR="${D}" prefix=/usr sysconfdir=/etc libdir="/usr/$(get_libdir)"
	escons
}

src_install() {
	escons DESTDIR="${D}" prefix=/usr sysconfdir=/etc libdir="/usr/$(get_libdir)" install
	einstalldocs
	#dosym "/usr/bin/sd_${PN}" "/usr/$(get_libdir)/speech-dispatcher-modules/sd_${PN}"
}
