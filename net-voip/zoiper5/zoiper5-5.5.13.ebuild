# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit rpm desktop xdg-utils

DESCRIPTION="Free VoIP softphone for non-commercial use"
HOMEPAGE="https://www.zoiper.com"

SRC_URI="Zoiper5_${PV}_x86_64.rpm"

LICENSE="EULA"
SLOT="0"
KEYWORDS="~amd64"
IUSE="pulseaudio"
RESTRICT="fetch binchecks bindist strip"

DEPEND=""
RDEPEND="${DEPEND}
	pulseaudio? ( media-sound/pulseaudio )
	app-crypt/mit-krb5
	media-libs/libv4l
	>=dev-libs/nss-3.22
	media-libs/freetype
	>=sys-libs/glibc-2.18
	x11-libs/libnotify
	x11-libs/libXScrnSaver
	x11-libs/libxkbcommon
"
BDEPEND=""

#QA_PREBUILT="*"
S="${WORKDIR}"

pkg_nofetch()
{
	einfo "Please download"
	for dist in ${A[@]}; do
		einfo " - $dist"
	done
	einfo "from https://www.zoiper.com/en/voip-softphone/download/zoiper5/for/linux-rpm"
	einfo "and place them into your DISTDIR"
}

src_unpack() {
	rpm_src_unpack ${A}
}

src_install() {
	cp -vR "${S}"/* "${D}"/
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}
